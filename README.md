# matrix-bot

A bot based on matrix-comander.

Allow to delete all messages from a list of users using the command `!wipe @user:homeserver.tld`. To run you need to setup the `Dockerfile` in the docker folder to match your desired room and managers (users who are allowed to run the `!wipe` command using your credentials)

This will be an instance of your account that will be reading for the `--manageroom` rooms message, to see if one of the `--managers` send a `!wipe` command. The wipe command doesn't ban the user, nor wipe the message itself.

## Matrix Commander

This bot is based on [matrix-comander](https://github.com/8go/matrix-commander) and requires you to log in using the `credentials.json` file used by this software.

